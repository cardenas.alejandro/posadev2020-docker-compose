# POSADEV 2020 - Consolidar Logs de Spring Boot en  Elastic Stack (ELK)

## Descripcion de archivos

- docker-compose.yml: Levanta la infraestructura requerida para correr los servicios incluyendo: elasticsearch, Logstash, Kibana, Filebeat, consul, mongodb
- docker-compose-nobeat.yml: Levanta la infraestructura requerida para correr los servicios incluyendo: elasticsearch, Logstash, Kibana, consul, mongodb
- docker-compose-all.yml: Levanta la infraestructura requerida incluyendo servicios: elasticsearch, Logstash, Kibana, Filebeat, consul, mongodb, order-service, inventory-service, api-gateway

## Iniciar docker-compose

Iniciar infraestructura: elasticsearch, logstash, kibana, filebeat, consul, mongodb

`docker-compose up`

Iniciar infraestructura: elasticsearch, logstash, kibana, consul, mongodb

`docker-compose -f docker-compose-nobeat.yml up`

Iniciar infraestructura: elasticsearch, logstash, kibana, filebeat, consul, mongodb, api-gateway, order-service, inventory-service

`docker-compose -f docker-compose-all.yml up`

## Logstash Pipelines

-   [filebeat.conf](logstash/pipeline/filebeat.conf)
-   [logback-json.conf](logstash/pipeline/logback-json.conf)
-   [filebeat.conf](logstash/pipeline/filebeat.conf)